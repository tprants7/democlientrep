import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestPageComponent } from './components/test-page/test-page.component';
import { FieldsComponent } from './components/fields/fields.component';
import { OneGameComponent } from './components/one-game/one-game.component';

const routes: Routes = [
  { path: "test", component : TestPageComponent},
  { path: "fields", component : FieldsComponent},
  { path: "game/:id", component : OneGameComponent} ,
  { path: "", redirectTo : "/test", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
