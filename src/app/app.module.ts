import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TestPageComponent } from './components/test-page/test-page.component';
import { FieldsComponent } from './components/fields/fields.component';
import { SquareComponent } from './components/square/square.component';
import { LineComponent } from './components/line/line.component';
import { FieldComponent } from './components/field/field.component';
import { OneGameComponent } from './components/one-game/one-game.component';

@NgModule({
  declarations: [
    AppComponent,
    TestPageComponent,
    FieldsComponent,
    SquareComponent,
    LineComponent,
    FieldComponent,
    OneGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
