import { Component, OnInit } from '@angular/core';
import { Field } from 'src/app/interfaces/field';
import { FieldService } from 'src/app/services/fieldService/field.service';
import { GameService } from 'src/app/services/gameService/game.service';

@Component({
  selector: 'app-fields',
  templateUrl: './fields.component.html',
  styleUrls: ['./fields.component.scss']
})
export class FieldsComponent implements OnInit {
  fields : Field[];
  newGameId : number;

  constructor(
    private fieldService : FieldService,
    private gameService : GameService
  ) { }

  ngOnInit() {
    this.getListOfFields();
  }

  public getListOfFields() {
    this.fieldService.getAllFields().subscribe(allFields => this.fields = allFields);
  }

  public startNewGame(mapId : number) {
    this.gameService.startNewGame(mapId).subscribe(gameId => this.newGameId = gameId);
  }

}
