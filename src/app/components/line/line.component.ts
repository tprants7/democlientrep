import { Component, OnInit, Input } from '@angular/core';
import { HorizontalLine } from 'src/app/interfaces/horizontal-line';

@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.scss']
})
export class LineComponent implements OnInit {
  @Input() oneLine : HorizontalLine;

  constructor() { }

  ngOnInit() {
  }

}
