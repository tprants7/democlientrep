import { Component, OnInit } from '@angular/core';
import { Game } from 'src/app/interfaces/game';
import { ActivatedRoute } from '@angular/router';
import { GameService } from 'src/app/services/gameService/game.service';

@Component({
  selector: 'app-one-game',
  templateUrl: './one-game.component.html',
  styleUrls: ['./one-game.component.scss']
})
export class OneGameComponent implements OnInit {
  private game : Game;

  constructor(
    private route : ActivatedRoute,
    private gameService : GameService
  ) { }

  ngOnInit() {
    this.getGame();
  }

  private getGame() {
    const id = +this.route.snapshot.paramMap.get("id");
    this.gameService.getGameByid(id).subscribe(foundGame => this.game = foundGame)
  }

}
