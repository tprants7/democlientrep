import { Component, OnInit, Input } from '@angular/core';
import { Square } from 'src/app/interfaces/square';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {
  @Input() oneSquare : Square;

  constructor() { }

  ngOnInit() {
  }

}
