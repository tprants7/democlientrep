import { Component, OnInit } from '@angular/core';
import { TestServiceService } from 'src/app/services/testService/test-service.service';
import { TestObject } from 'protractor/built/driverProviders';

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.scss']
})
export class TestPageComponent implements OnInit {
  public testMessage : TestObject;

  constructor(
    private testService : TestServiceService
  ) { }

  ngOnInit() {
    this.getMessage();
  }

  private getMessage() {
    this.testService.getTestMessage().subscribe(message => this.testMessage = message);
  }

}
