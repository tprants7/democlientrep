import { HorizontalLine } from './horizontal-line';

export interface Field {
    lines : HorizontalLine[];
    name : String;
    id : number;
}
