import { Field } from './field';
import { Square } from './square';

export interface Game {
    id : number;
    map : Field,
    location : Square
}
