import { Square } from './square';

export interface HorizontalLine {
    squares : Square[];
    y : number;
}
