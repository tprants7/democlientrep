import { TestBed } from '@angular/core/testing';

import { BaseAddressService } from './base-address.service';

describe('BaseAddressService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BaseAddressService = TestBed.get(BaseAddressService);
    expect(service).toBeTruthy();
  });
});
