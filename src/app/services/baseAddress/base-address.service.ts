import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseAddressService {
  private baseUrl = "http://localhost:8080";

  constructor() { }

  public getBaseAddress() : String {
    return this.baseUrl;
  }
}
