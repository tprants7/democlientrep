import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseAddressService } from '../baseAddress/base-address.service';
import { Observable } from 'rxjs';
import { Field } from 'src/app/interfaces/field';

@Injectable({
  providedIn: 'root'
})
export class FieldService {
  private url = "field"

  constructor(
    private http : HttpClient,
    private baseService : BaseAddressService
  ) { }

  public getAllFields() : Observable<Field[]> {
    const fullUrl = `${this.baseService.getBaseAddress()}/${this.url}`;
    return this.http.get<Field[]>(fullUrl);
  }

  public getFieldById(id : number) : Observable<Field> {
    const fullUrl = `${this.baseService.getBaseAddress()}/${this.url}/${id}`;
    return this.http.get<Field>(fullUrl);
  }
}
