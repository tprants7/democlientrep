import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseAddressService } from '../baseAddress/base-address.service';
import { Observable } from 'rxjs';
import { Game } from 'src/app/interfaces/game';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private url = "game";

  constructor(
    private http : HttpClient,
    private baseService : BaseAddressService
  ) { }

  public startNewGame(mapId : number) : Observable<number> {
    const fullUrl = `${this.baseService.getBaseAddress()}/${this.url}/newGame`;
    return this.http.post<number>(fullUrl, mapId);
  }

  public getGameByid(id : number) : Observable<Game> {
    const fullUrl = `${this.baseService.getBaseAddress()}/${this.url}/${id}`;
    return this.http.get<Game>(fullUrl);
  }
}
