import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseAddressService } from '../baseAddress/base-address.service';
import { Observable } from 'rxjs';
import { TestObject } from 'protractor/built/driverProviders';

@Injectable({
  providedIn: 'root'
})
export class TestServiceService {
  private url = "test";

  constructor(
    private http : HttpClient,
    private baseService : BaseAddressService
  ) { }

  public getTestMessage() : Observable<TestObject> {
    const fullUrl = `${this.baseService.getBaseAddress()}/${this.url}`;
    return this.http.get<TestObject>(fullUrl);
  }
}
